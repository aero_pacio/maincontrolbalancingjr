#ifndef __COMMS_H__
#define __COMMS_H__

#include "stdint.h"

typedef struct{
    uint16_t motR;
    uint16_t motL;
    uint8_t enable;
    uint8_t orden_code;
    uint16_t checksum;
}tx_data;

void uart_init(void);

#endif