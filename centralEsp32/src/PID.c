#include "PID.h"
#include "stdio.h"

const uint8_t periodoPID=10;  						//periodo cada el cual se ejecuta el PID en ms

float angle_min, angle_max;
float ITerm, lastInput;
uint16_t contador_timer=0;

struct S_pid PID_n1;

// extern TIM_HandleTypeDef htim7;                  // TODO: establecer un timer para un periodo preciso



void pidInit(void){																			//inicializo variables y el timer 2 para calcular el pid en procesos regulares de 100ms

//	set_ajuste_pid1(0.07,0.00,0.00);											//deberia recuperar estas variables de la EEPROM
	
/* configuro el TIM7 que es un basic timer para generar un desborde cada 10ms, donde se atenderan los calculos del PID*/
	
    /* TODO: establecer un timer para un periodo preciso*/
//	clf/freq=(1+ARR)(1+PSC)
//	64000000/100=(1+64000)(1+PSC) +> PSC=9
	// TIM7->ARR=64001;
	// TIM7->PSC=9;
	// TIM7->CNT=0;

	// HAL_TIM_Base_Start(&htim7);
	// HAL_TIM_Base_Start_IT(&htim7);
	// HAL_NVIC_EnableIRQ(TIM7_IRQn);

}

void pidEnable(void){
	
	PID_n1.enablePID = 1;
}

void pidDisable(void){
	
	PID_n1.enablePID = 0;
}

/*
 * Esta funcion es llamada desde el TIM7 a un periodo fijo de tiempo
 */
float pidCalculate(float input){									//TODO: HACER REFACTOR
	
	if(!PID_n1.enablePID){
		 return 0;													//TODO: REVISAR ESTA MAL
	}
	PID_n1.input=input;
	double error = PID_n1.set_angle - PID_n1.input;											//calculo P: resto error entre el valor seteado y el de entrada 		
	ITerm+= (PID_n1.ki * error);															//calculo I: acumulo error ya multiplicado por ki

	if(ITerm> angle_max){																	//recorto el termino integral maximo
			ITerm= angle_max;
		}
		else if(ITerm< angle_min){															//recorto el termino intgral minimo
			ITerm= angle_min;
		}
		double dInput = (PID_n1.input - lastInput);											//Calculo D: resto la entrada anterior a la actual

		/*Compute PID Output*/
		PID_n1.output = (PID_n1.kp * error) + ITerm - (PID_n1.kd * dInput);						//opero con los 3 parametros para obtener salida, el D se resta para evitar la kick derivate
		if(PID_n1.output > angle_max){														//recorto la salida máxima
			PID_n1.output = angle_max;
		}
		else if(PID_n1.output < angle_min){													//recorto la salida minima
			PID_n1.output = angle_min;
		}

		lastInput = PID_n1.input;

	return PID_n1.output;
}
 
void pidSetPointAngle(float angulo){
	PID_n1.set_angle=angulo;
}

/*
 * 	Funcion para cargar los parametros al filtro PID
 *	Todos los parametros deben estar entre 0.00 y 1.00
*/ 
void pidSetKs(float KP,float KI,float KD){

  double SampleTimeInSec = ((double)periodoPID)/1000;
  PID_n1.kp = KP;
  PID_n1.ki = KI * SampleTimeInSec;																 
  PID_n1.kd = KD / SampleTimeInSec;
}
 
 /* 
 * Funcion para limitar la salida del Filtro PID
 * si se encuentra en runtime primero recorto los parametros PID
 * para adecuarlos a los nuevos maximos y minimos
 */
void pidSetLimits(float min, float max){	                // TODO: modificar para setear el minimo y el maximo de la salida, no un angulo???

    if(min > max){
        return;
    }
    angle_min = min;
    angle_max = max;

    if(PID_n1.output > angle_max){
        PID_n1.output = angle_max;
    }
    else if(PID_n1.output < angle_min){
        PID_n1.output = angle_min;
    }

    if(ITerm> angle_max){
        ITerm= angle_max;
    }
    else if(ITerm< angle_min){
        ITerm= angle_min;
    }
}

