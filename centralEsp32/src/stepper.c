#include "stepper.h"
#include "rom/ets_sys.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_task_wdt.h"

#define STEP_L     19      
#define STEP_R     23
#define EN_MOT     18
#define DIR_L      21
#define DIR_R      22


#define US_MIN  100
#define US_MAX  500
#define US_DIFF (US_MAX-US_MIN)

#define CPU_STEPPER     1

int16_t pwmr=0,pwml=0;                  // almacena la velocidad en terminos de us
uint8_t enableVelMot=0;


TaskHandle_t vHandleSteppers;

static void vTaskMotors(void *pvParameters);
static uint16_t vel2us(int16_t vel);

void motorsInit(void){
    /* seteo pines de salida de steps */
    gpio_config_t pinesMotor={
        .intr_type = GPIO_INTR_DISABLE,
        .mode = GPIO_MODE_OUTPUT,
        .pin_bit_mask = ( 1 << STEP_L),
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .pull_up_en = GPIO_PULLUP_DISABLE
    };
    gpio_config(&pinesMotor);
    gpio_pad_select_gpio(STEP_L);

    pinesMotor.pin_bit_mask = (1 << STEP_R);
    gpio_config(&pinesMotor);

    /* seteo pines de salida de enable*/
    pinesMotor.pin_bit_mask = (1 << EN_MOT);
    gpio_config(&pinesMotor);

    /* seteo pines de salida de direccion*/
    pinesMotor.pin_bit_mask = (1 << DIR_L);
    gpio_config(&pinesMotor);
    pinesMotor.pin_bit_mask = (1 << DIR_R);
    gpio_config(&pinesMotor);

    gpio_set_level(EN_MOT,1);

    printf("LANZANDO MOTOR RUNNING...\n");
    xTaskCreatePinnedToCore(vTaskMotors,"Tarea Stepper",2048,NULL,3,&vHandleSteppers,CPU_STEPPER);
}


void enableMotors(void){
    gpio_set_level(EN_MOT,0);
    printf("Enable motors\n");
}
void disableMotors(void){
    gpio_set_level(EN_MOT,1);
    printf("disable motors\n");
}

static uint16_t vel2us(int16_t vel){
    uint16_t velReal = abs(vel)/10;                         //obtengo el valor absoluto dividido por 10, de esta manera lo convierto a porcentaje

    if(vel != 0 ){
        enableVelMot=1;      
    }
    else{                                                   // si la velocidad es 0 deshabilito los pulsos step
//        enableVelMot=0;
    }
    return US_MIN+(((100-velReal) * US_DIFF) / 100);
}

void setVelMotors(int16_t speedL,int16_t speedR){

    if(speedL <1000 && speedL> -1000){
        pwml = vel2us(speedL);
        if(speedL < 0){
            gpio_set_level(DIR_L,1);
        }
        else{
            gpio_set_level(DIR_L,0);
        }
    }
    else{
 //       enableVelMot=0;
    }

    if(speedR <1000 && speedR> -1000){
        pwmr = vel2us(speedR);
        if(speedR < 0){
            gpio_set_level(DIR_R,0);
        }
        else{
            gpio_set_level(DIR_R,1);
        }
    }
    else{
//        enableVelMot=0;
    }

    // printf("Vel: %d , enableVel: %d\n",pwmr,enableVelMot);
}


static void vTaskMotors(void *pvParameters){

    esp_task_wdt_init(1000,NULL);
    esp_task_wdt_add(NULL);

    for(;;){

        if(enableVelMot){

            gpio_set_level(STEP_L,1);
            gpio_set_level(STEP_R,1);
            ets_delay_us(pwmr);             //TODO: independizar velocidades
        }
        gpio_set_level(STEP_L,0);
        gpio_set_level(STEP_R,0);
        ets_delay_us(pwmr);                 //TODO: independizar velocidades
        
    }
}