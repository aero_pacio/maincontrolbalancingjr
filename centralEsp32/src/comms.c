#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "esp_log.h"

#define BAUDRATE_CONTROL 115200

#define TX_PIN     0
#define RX_PIN     1

#define UART_TAG "UART CONTROL"
const uart_port_t PORT_CONTROL = UART_NUM_0;

const int uart_buf_size = 100;
QueueHandle_t   uartQueue;


void uart_init(){
    esp_err_t ret;
    uart_config_t   config_port_control={
        .baud_rate = BAUDRATE_CONTROL,
        .data_bits  =  UART_DATA_8_BITS ,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE ,
        .parity = UART_PARITY_DISABLE ,
        .rx_flow_ctrl_thresh = 122,                 // <- no se que es, estaba en la documentacion
        .source_clk = UART_SCLK_APB,
        .stop_bits = UART_STOP_BITS_1,
 //       .use_ref_tick = ,                         // <- deprecado
    };

    if((ret = uart_param_config(PORT_CONTROL,&config_port_control)) != ESP_OK){
        ESP_LOGE(UART_TAG,"ERROR FUNCION: %s, ERROR: %s",__func__,esp_err_to_name(ret));
    }

    if((ret = uart_set_pin(PORT_CONTROL,TX_PIN,RX_PIN,0,0)) != ESP_OK){                     // TODO: reemplazar los 0,0 
        ESP_LOGE(UART_TAG,"ERROR FUNCION: %s, ERROR: %s",__func__,esp_err_to_name(ret));
    }
       
    if((ret = uart_driver_install(PORT_CONTROL,uart_buf_size,uart_buf_size, 10,&uartQueue,0)) != ESP_OK){ // TODO: falta determinar queueSize e interrupts allocate
        ESP_LOGE(UART_TAG,"ERROR FUNCION: %s, ERROR: %s",__func__,esp_err_to_name(ret));
    }

    /* muestra envio */ 
    uint16_t VEC_TX[10];
    uart_write_bytes(PORT_CONTROL,&VEC_TX, sizeof(VEC_TX));

    /* muestra recepcion */
    uint8_t data[128];
    int length = 0;
    ESP_ERROR_CHECK(uart_get_buffered_data_len(PORT_CONTROL, (size_t*)&length));
    length = uart_read_bytes(PORT_CONTROL, data, length, 100);
    uart_flush(PORT_CONTROL);   //limpia el/los buffer
}