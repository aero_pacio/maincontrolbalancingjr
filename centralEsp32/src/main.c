#include "main.h"
#include "driver/i2c.h"
#include "driver/gpio.h"
#include "freertos/task.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_task_wdt.h"
#include "mpu6050.h"
#include "bt_module.h"
#include "PID.h"
#include "stepper.h"
#include "esp_log.h"

float valMotores=0.00;
void vTaskPID(void *pvParameters){
    static uint8_t toggle=0,cont=0;
    pidInit();
    pidSetLimits(-100.00,100.00);                 // TODO: cargar limites de la FLASH
    pidSetKs(0.8,0,0.15);                              // TODO: cargar parametros PID desde la flash
    pidSetPointAngle(0.00);                         //angulo de equilibrio
    pidEnable();

    for(;;){

        //if(queueNew){                             // TODO: solo debe ejecutarse si hay elementos disponibles en la cola
        /* inicio cuenta demora calculo pid*/
        valMotores=pidCalculate(getAngle(AXIS_ANGLE_Z));// TODO: CHEQUEAR EJE DE EQUILIBRIO, TODO: la salida se da en angulos limitados por pidSetLimits
        setVelMotors((int16_t)valMotores*-15,(int16_t)valMotores*-15);

        // if((getAngle(AXIS_ANGLE_Z)<5.00 && getAngle(AXIS_ANGLE_Z)>-5.00)){
        //     disableMotors();
        // }
        // else{
        //     enableMotors();
        // }
        /* finalizo cuenta demora calculo pid: 50 us*/

        // if(toggle){
        //     toggle=0;
        // }
        // else{
        //     toggle=1;
        // }
        // gpio_set_level(LED1,toggle);

        vTaskDelay(pdMS_TO_TICKS(1));               // esta tarea se ejecuta cada 10ms
        if(cont){
            cont--;
        }
        else{
            //printf("Z: %f, Motores: %f\n",getAngle(2),valMotores*15);
            cont=10;
        }
        //}
    }
}

void sendDataMpu(void *pvParameters){
    for(;;){
        if(btIsConnected()){
             btSendAngle(getAngle(0),getAngle(1),getAngle(2));
             //btSendData(getAngle(0),getAngle(1),(uint16_t)valMotores);
        }
        printf("X: %f , Motores: %d\n",getAngle(0),(uint16_t)valMotores);

        vTaskDelay(pdMS_TO_TICKS(10));
    }
}

void app_main(void){

    motorsInit();
    disableMotors();

     /* seteo pines de salida de debug */
    gpio_config_t pinesLeds={
        .intr_type = GPIO_INTR_DISABLE,
        .mode = GPIO_MODE_OUTPUT,
        .pin_bit_mask = ( 1 << LED1),
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .pull_up_en = GPIO_PULLUP_DISABLE
    };
    gpio_config(&pinesLeds);
    pinesLeds.pin_bit_mask = ( 1<< LED2);
    gpio_config(&pinesLeds);

    esp_err_t ret = nvs_flash_init();
    if(ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND){
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    // printf("inicializando BT!\n");

    // if(bt_init() == ESP_OK){
    //     printf("BT inicializado correctamente\n");
    // }


    vTaskDelay(1000);                                //doy tiempo a que inicie el mpu6050
    printf("inicializando MPU!\n");
    mpu_init();

    printf("HABILITANDO MOTORES\n");
    enableMotors();
    
    printf("iniciando tareas..\n");

    xTaskCreatePinnedToCore(sendDataMpu,"envio MPU", 2048,NULL,2,NULL,1);
    xTaskCreatePinnedToCore(vTaskPID,"tarea PID", 2048,NULL,5,NULL,1);

    printf("tareas corriendo!\n");

    // ESP_LOGE(tag_main,"log",null);
}


